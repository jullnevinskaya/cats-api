import Client from '../../dev/http-client';
import {Reporter, Severity} from "jest-allure/dist/Reporter";

const HttpClient = Client.getInstance();

declare const reporter: Reporter;

describe('API Core / allByLetter', () => {

    it('Получение списка котов, сгруппированных по группам', async () => {

        reporter.severity(Severity.Critical);
        reporter.description('Тест на получение списка котов по группам');
        reporter.startStep('Запрос на получение списка котов');

        const response = await HttpClient.get(`core/cats/allByLetter`, {
            responseType: 'json',
        });

        reporter.addAttachment(
            'testAttachment',
            JSON.stringify(response.body, null, 2),
            'application/json'
        );
        reporter.endStep();

        reporter.startStep('Делаю проверку статуса 200');
        expect(response.statusCode).toEqual(200);
        reporter.endStep();

        // Проверка групп
        reporter.startStep('Делаю проверку структуры ответа');
        expect(response.body).toEqual({
            groups: expect.arrayContaining([
                expect.objectContaining({
                    count_in_group: expect.any(Number),
                    count_by_letter: expect.any(Number),
                    title: expect.any(String),
                    cats: expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            description: expect.any(String),
                            tags: null,
                            gender: expect.any(String),
                            likes: expect.any(Number),
                            dislikes: expect.any(Number),
                        }),
                    ]),
                }),
            ]),
            count_output: expect.any(Number),
            count_all: expect.any(Number),
        });
        reporter.endStep();
    });
});
