import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';
import {Reporter, Severity} from "jest-allure/dist/Reporter";

declare const reporter: Reporter;

const cats: CatMinInfo[] = [{ name: 'Примерюлиногокота', description: 'Очень милый котик', gender: 'female' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('API Core / get-by-id', () => {
  beforeAll(async () => {
    try {
      const add_cat_response = await HttpClient.post('core/cats/add', {
        responseType: 'json',
        json: { cats },
      });
      if ((add_cat_response.body as CatsList).cats[0].id) {
        catId = (add_cat_response.body as CatsList).cats[0].id;
      } else throw new Error('Не получилось получить id тестового котика!');
    } catch (error) {
      throw new Error('Не удалось создать котика для автотестов!');
    }
  });

  afterAll(async () => {
    await HttpClient.delete(`core/cats/${catId}/remove`, {
      responseType: 'json',
    });
  });

  it('Поиск существующего котика по id', async () => {
    reporter.severity(Severity.Critical);
    reporter.description('Тест на поиск существующего котика по id');
    reporter.startStep('Запрос на получение ранее добавленного котика');

    const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
      responseType: 'json',
    });

    reporter.addAttachment(
        'testAttachment',
        JSON.stringify(response.body, null, 2),
        'application/json'
    );
    reporter.endStep();

    reporter.startStep('Делаю проверку статуса');
    expect(response.statusCode).toEqual(200);
    reporter.endStep();

    reporter.startStep('Делаю проверку структуры ответа');
    expect(response.body).toEqual({cat: {
      id: catId,
      ...cats[0],
      tags: null,
      likes: 0,
      dislikes: 0,
    }});
    reporter.endStep();
  });

  it('Поиск некорректного id котика', async () => {
    reporter.severity(Severity.Normal);
    reporter.description('Тест на поиск котика с некорректным id');
    reporter.startStep('Ответ 404 на запрос котика с невалидным значением ID');

    await expect(
        HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
          responseType: 'json',
        })
    ).rejects.toThrowError('Response code 400 (Bad Request)');
    reporter.endStep();

  });
});
